#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_EXTRACT_NUMBERS
#endif
#include <boost/test/unit_test.hpp>

#include "run.hpp"

BOOST_AUTO_TEST_SUITE(ExtractNumbersSuite)

BOOST_AUTO_TEST_CASE(testNoNumbers)
{
	std::unique_ptr<std::vector<int>> numbers = extractNumbers("");

	BOOST_CHECK(numbers->empty());
}

BOOST_AUTO_TEST_CASE(testOneNumber)
{

	std::unique_ptr<std::vector<int>> numbers = extractNumbers("1");

	BOOST_CHECK(numbers->at(0) == 1);
}

BOOST_AUTO_TEST_CASE(testMoreNumbers)
{

	std::unique_ptr<std::vector<int>> numbers = extractNumbers("1,2,10");

	std::vector<int> actual = { 1, 2, 10 };

	BOOST_CHECK(*numbers == actual);
}

BOOST_AUTO_TEST_SUITE_END()


CC := g++
SRCDIR := src managed-ipt-common/src
TESTDIR := test
BUILDDIR := build
BINDIR := bin
TARGET := $(BINDIR)/managed-ipt-admin
TEST_TARGET := $(BINDIR)/tester
TEST_MAIN := $(TESTDIR)/tester.cpp

SRCEXT := cpp
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
TEST_SOURCES := $(shell find $(TESTDIR) -type f -name *.$(SRCEXT))
# .cpp -> o, same nazwy, dopisuję katalog budowania
OBJECTS := $(addprefix $(BUILDDIR)/,$(notdir $(SOURCES:.$(SRCEXT)=.o)))
OBJECTS_NO_MAIN := $(filter-out $(BUILDDIR)/main.o,$(OBJECTS))
CFLAGS := -std=c++11 -g -Wall -Wextra -DBOOST_LOG_DYN_LINK
LIB := -pthread -lboost_unit_test_framework -lboost_program_options -lboost_log -lboost_log_setup -lboost_thread -lboost_system -lcryptopp 
INC := $(foreach path,$(SRCDIR),-I $(path))

$(TARGET): $(OBJECTS)
	@mkdir -p $(BINDIR)
	$(CC) $^ -o $(TARGET) $(LIB)

$(BUILDDIR)/%.o: $(firstword $(SRCDIR))/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

$(BUILDDIR)/%.o: $(lastword $(SRCDIR))/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

tester: $(TEST_TARGET)

$(TEST_TARGET): $(OBJECTS_NO_MAIN) $(TEST_MAIN) $(TEST_SOURCES)
	@mkdir -p $(BINDIR)
	$(CC) $(CFLAGS) $(INC) $^ -o $(TEST_TARGET) $(LIB)
	$(TEST_TARGET)

clean:
	$(RM) -r $(BUILDDIR) $(BINDIR)

.PHONY: clean

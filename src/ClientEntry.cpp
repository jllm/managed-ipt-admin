/*
 * ClientEntry.cpp
 *
 *  Created on: 3 maj 2014
 *      Author: jakub
 */

#include "ClientEntry.hpp"

ClientEntry::ClientEntry(const uint32_t port, const std::string& ip, const CryptoPP::RSA::PublicKey& publicKey)
 : port(port), ip(ip), publicKey(publicKey)
{

}

ClientEntry::~ClientEntry()
{

}

int ClientEntry::getPort() const
{
	return port;
}

const std::string& ClientEntry::getIp() const
{
	return ip;
}

const CryptoPP::RSA::PublicKey& ClientEntry::getPublicKey() const
{
	return publicKey;
}

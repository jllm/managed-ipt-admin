/*
 * ClientEntry.hpp
 *
 *  Created on: 3 maj 2014
 *      Author: jakub
 */

#pragma once

#include "cryptopp/rsa.h"
#include <cstdint>
#include <string>

class ClientEntry
{
	private:

		/** Port na którym nasłuchuje klient. */
		const uint32_t port;
		// TODO: Zmienić na klasę IP
		/** Adres IP klienta. */
		const std::string ip;
		/** Klucz publiczny klienta. */
		const CryptoPP::RSA::PublicKey publicKey;

	public:

		ClientEntry(const uint32_t, const std::string&, const CryptoPP::RSA::PublicKey&);
		~ClientEntry();
		int getPort() const;
		const std::string& getIp() const;
		const CryptoPP::RSA::PublicKey& getPublicKey() const;
};


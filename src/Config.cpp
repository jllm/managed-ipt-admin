/*
 * Config.cpp
 *
 *  Created on: 3 maj 2014
 *      Author: jakub
 */

#include "Config.hpp"

Config::Config(const std::string& adminPrivKeyFile, const std::string& adminPubKeyFile,
		const std::string& clientsListFile)
:
		adminPrivKeyFile(adminPrivKeyFile), adminPubKeyFile(adminPubKeyFile), clientsListFile(clientsListFile)
{

}

Config::~Config()
{

}

std::string Config::getClientsListFile() const
{
	return clientsListFile;
}

std::string Config::getAdminPrivateKeyFile() const
{
	return adminPrivKeyFile;
}

std::string Config::getAdminPublicKeyFile() const
{
	return adminPubKeyFile;
}

#include "TCPClient.hpp"

TCPClient::TCPClient(const std::string& ipAddress, int portNumber)
	: ipAddress(ipAddress), portNumber(portNumber)
{
}

std::string TCPClient::sendAndGetReply(const std::string& request, const int timeoutSec)
{
	int sock = doConnection();

	write(sock, request.c_str(), request.size());

	int rval;
	size_t endOfPacketSignIndex;
	std::string packetBuffer;

	timeval timeout;
	timeout.tv_sec = timeoutSec;
	timeout.tv_usec = 0;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*) &timeout, sizeof(timeval));
	while (true)
	{
		rval = read(sock, readBuffer, BUFFER_SIZE - 1);

		if(rval <= 0)
			break;

		readBuffer[rval] = 0;

		packetBuffer += readBuffer;

		if(packetBuffer.length() > BUFFER_SIZE)
		{
			break;  //niepoprawny pakiet
		}

		if((endOfPacketSignIndex = packetBuffer.find_first_of("$")) != std::string::npos)
		{
			packetBuffer = packetBuffer.substr(0, endOfPacketSignIndex + 1);  //gdy cos jeszcze jest to wyrzucamy
			break;
		}
	}
	close(sock);

	return packetBuffer;
}

int TCPClient::doConnection()
{
	int sock = socket(AF_INET6, SOCK_STREAM, 0);
	if (sock == -1)
	{
		throw SocketException();
	}

	sockaddr_in6 agent_name;
	agent_name.sin6_flowinfo = 0;
	agent_name.sin6_family = AF_INET6;
	agent_name.sin6_addr = in6addr_any;
	inet_pton(AF_INET6,ipAddress.c_str(), &agent_name.sin6_addr);
	agent_name.sin6_port = htons(portNumber);

	int status = connect(sock, (struct sockaddr*) &agent_name, sizeof(agent_name));
	if (status == -1)
	{
		throw ConnectException();
	}

	return sock;
}


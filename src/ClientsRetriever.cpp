#include "ClientsRetriever.hpp"

ClientsRetriever::ClientsRetriever(const std::string& f) :
		file(f)
{};

std::unique_ptr<std::vector<ClientEntry>> ClientsRetriever::retrieve()
{
	std::unique_ptr<std::vector<ClientEntry>> clients(new std::vector<ClientEntry>());

	std::fstream in;
	in.open(file.c_str(), std::fstream::in);

	if(in.fail())
	    return clients;

	boost::char_separator<char> sep(" *", "", boost::drop_empty_tokens); //separator '*'
	std::string buf;
	uint32_t port;
	std::string ip;
	std::string publicKeyBase64Encoded;
	CryptoPP::RSA::PublicKey publicKey;
	boost::tokenizer<boost::char_separator<char> > tok(buf, sep);
	boost::tokenizer<boost::char_separator<char> >::iterator iterEnd;

	while (!in.eof() && !std::getline(in, buf).eof())
	{
		tok.assign(buf, sep);
		boost::tokenizer<boost::char_separator<char> >::iterator iter =
				tok.begin();
		if (iter == iterEnd)
		{
			in.close();
			throw std::runtime_error("I/O error");
		}
		port = boost::lexical_cast<uint32_t>(*iter);
		++iter;
		if (iter == iterEnd)
		{
			in.close();
			throw std::runtime_error("I/O error");
		}
		ip = *iter;
		++iter;

		if (iter == iterEnd)
		{
			in.close();
			throw std::runtime_error("I/O error");
		}
		publicKeyBase64Encoded = *iter;
		++iter;

		if (iter != iterEnd)
		{
			in.close();
			throw std::runtime_error("Too much data in one row");
		}

		//odkodowanie klucza
		CryptoPP::ByteQueue byteQueueBase64;
		byteQueueBase64.Put(
				reinterpret_cast<const byte*>(publicKeyBase64Encoded.c_str()),
				publicKeyBase64Encoded.length());
//		byteQueueBase64.MessageEnd();

		CryptoPP::Base64Decoder decoder;
		byteQueueBase64.CopyTo(decoder);
		publicKey.Load(decoder);
		clients->push_back(ClientEntry(port, ip, publicKey));
	}
	in.close();
	return clients;
}

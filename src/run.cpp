/*
 * run.cpp
 *
 *  Created on: 2 maj 2014
 *      Author: jakub
 */

#include "run.hpp"

namespace
{
	Logger logger;
}

void check_dependency(const boost::program_options::variables_map& vm)
{
	namespace c = common::program_options;
	c::option_dependency(vm, "list-clients", "clients");
}

void check_conflicts(const boost::program_options::variables_map& vm)
{
	namespace c = common::program_options;
	c::conflicting_options(vm, "append", "delete");
	c::conflicting_options(vm, "append", "insert");
	c::conflicting_options(vm, "insert", "delete");
	c::conflicting_options(vm, "append", "list");
	c::conflicting_options(vm, "insert", "list");
	c::conflicting_options(vm, "delete", "list");
	c::conflicting_options(vm, "force-setup", "setup");
}

void listClients(const std::string& clientsFile)
{
	ClientsRetriever cr(clientsFile);
	std::unique_ptr<std::vector<ClientEntry>> clients = cr.retrieve();

	int i = 1;
	for (const ClientEntry& client : *clients)
	{
		std::cout << i << ": " << client.getIp() << " " << client.getPort() << std::endl;
		++i;
	}
}

std::unique_ptr<std::vector<int>> extractNumbers(const std::string& str)
{
	std::vector<int>* numbers = new std::vector<int>();

	if (str != "")
	{
		std::vector<std::string> splitVec;
		boost::split(splitVec, str, boost::is_any_of(","));
		std::transform(
				splitVec.begin(),
				splitVec.end(),
				std::back_inserter(*numbers),
				// FIXME: jeśli ktoś wie jak inaczej przekazać funkcję, niż jako lambdę to może zmienić.
				[](const std::string& s)
				{
					return std::stoi(s);
				});
	}

	return std::unique_ptr<std::vector<int>>(numbers);
}

std::array<std::string, 5> createRequestParams(boost::program_options::variables_map vm, std::string sign)
{
	std::string operation;
	std::string options = "";
	std::string chainrulenum;
	std::string table = vm["table"].as<std::string>();
	if (vm.count("append"))
	{
		operation = "A";
		chainrulenum = vm["append"].as<std::string>();
	}
	else if (vm.count("delete"))
	{
		std::vector<std::string> s = vm["delete"].as<std::vector<std::string>>();
		if (s.size() <= 2)
		{
			operation = "D";
			chainrulenum = s[0];
			if (s.size() == 2)
			{
				chainrulenum += " " + s[1];
			}
		}
		else
		{
			throw boost::program_options::error_with_option_name(
					"option '%canonical_option%' takes only up to two arguments",
					"delete");
		}
	}
	else if (vm.count("insert"))
	{
		std::vector<std::string> s = vm["insert"].as<std::vector<std::string>>();
		if (s.size() <= 2)
		{
			operation = "I";
			chainrulenum = s[0];
			if (s.size() == 2)
			{
				chainrulenum += " " + s[1];
			}
		}
		else
		{
			throw boost::program_options::error_with_option_name(
					"option '%canonical_option%' takes only up to two arguments",
					"insert");
		}
	}
	else if (vm.count("list"))
	{
		operation = "L";
		chainrulenum = vm["list"].as<std::string>();
	}
	if (vm.count("options"))
	{
		std::vector<std::string> opts = vm["options"].as<std::vector<std::string>>();
		for (std::string s : opts)
		{
			options += s + " ";
		}
		options = options.substr(0, options.length() - 1);
	}
	std::array<std::string, 5> params
	{
			{
					table,
					operation,
					chainrulenum,
					options,
					sign
			}
	};
	return params;
}

int run(int argc, char* argv[])
{
	namespace po = boost::program_options;
	namespace cn = common;

	try
	{
		cn::log::init();

		std::string programName("managed-ipt-admin");
		std::string usage(
				"Usage:"
						"\t" + programName
						+ " -c clients -k admin-private-key [-t table] -[AD] chain -o \"rule-specification and iptables options\"\n"
								"\t" + programName
						+ " -c clients -k admin-private-key [-t table] -I chain [rulenum] -o \"rule-specification and iptables options\"\n"
								"\t" + programName
						+ " -c clients -k admin-private-key [-t table] -D chain rulenum [-o \"iptables options\"]\n"
								"\t" + programName + " -c clients -k admin-private-key -L [chain [rulenum]] [-o \"iptables options\"]\n"
								"\n"
								"\t" + programName + " -c clients --list-clients\n"
								"\t" + programName + " --setup | --force-setup\n");

		std::string filename("file");
		std::string timeout("timeout");
		po::options_description general("General options");
		general.add_options()
		("help,h",
				"Produce help message.")
		("setup,s", po::bool_switch()->default_value(false),
				"Setup program. Produce default config file, new keys, empty clients list file.\n"
						"Doesn't override existing files.")
		("clients,c", po::value<std::string>()->value_name(filename)->required(),
				"A file with client list.")
		("private-key,k", po::value<std::string>()->value_name(filename)->required(),
				"A file with admin's private key")
		("list-clients", po::bool_switch()->default_value(false),
				"Print numbered clients")
		("client-numbers", po::value<std::string>()->value_name("client-numbers"),
				"Clients to which a command should be sent, separated by commas")
		("timeout", po::value<int>()->value_name(timeout)->default_value(10),
						"Period for receiving an answer from agent in seconds")
				;

		po::options_description special("Special options");
		special.add_options()
		("debug", po::bool_switch()->default_value(false),
				"Print debug logs.")
		("force-setup,S", po::bool_switch()->default_value(false),
				"Forced setup (override existing files). See --setup.")
		("full-help,H",
				"Produce full help message.")
				;

		std::string chain("chain");
		std::string chainrulnum("chain [rulnum]");
		std::string table("table");
		std::string options("options");

		po::options_description ipt("Iptables");
		ipt.add_options()
		("append,A", po::value<std::string>()->value_name(chain),
				"Append chain.")
		("insert,I", po::value<std::vector<std::string> >()->value_name(chainrulnum)->multitoken(),
				"Insert in chain as rulenum (1 = first).") // multitoken for "-I chain rulenum"
		("delete,D", po::value<std::vector<std::string> >()->value_name(chainrulnum)->multitoken(),
				"Rulenum isn't specified:\n"
						"  Delete matching rule from chain.\n"
						"Rulenum is specified:\n"
						"  Delete rule rulenum (1 = first) from chain.") // multitoken for "-D chain rulenum"
		("list,L", po::value<std::string>()->implicit_value("")->value_name(chain),
				"List chain")
		("table,t", po::value<std::string>()->value_name(table)->required()->default_value(std::string("filter")),
				"Table to manipulate.")
		("options,o", po::value<std::vector<std::string> >()->value_name(options)->multitoken(),
				"Rule specification and other iptables options. Check iptables help.")
				;

		po::options_description all("Allowed options");
		all.add(general).add(special).add(ipt);

		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).
				options(all).run(), vm);

		if (vm.count("help") || argc == 1)
		{
			std::cout << programName << " v0.90\n\n";
			std::cout << usage << "\n";
			std::cout << general << "\n" << ipt;
			return EXIT_SUCCESS;
		}

		if (vm.count("full-help"))
		{
			std::cout << usage << "\n";
			std::cout << all;
			return EXIT_SUCCESS;
		}

		check_conflicts(vm);
		check_dependency(vm);

		if (vm["debug"].as<bool>())
		{
			cn::log::set_severity_level(debug);
		}

		if (vm["setup"].as<bool>() || vm["force-setup"].as<bool>())
		{
			Setup setup(vm["force-setup"].as<bool>());
			try
			{
				setup.run();
			}
			catch (std::logic_error& e)
			{
				BOOST_LOG_SEV(logger, error)<< "Error occurred while running setup.";
				cn::print_exception(logger, e);
				return EXIT_FAILURE;
			}
			return EXIT_SUCCESS;
		}

		if (vm["list-clients"].as<bool>())
		{
			listClients(vm["clients"].as<std::string>());
			return EXIT_SUCCESS;
		}

		// NOTIFY
		po::notify(vm);

		if (vm.count("append") || vm.count("delete") || vm.count("insert") || vm.count("list"))
		{
			try
			{
				std::string clientsPath = vm["clients"].as<std::string>();
				std::string privateKeyPath = vm["private-key"].as<std::string>();
				Config config(privateKeyPath, "", clientsPath);

				uint32_t id = 0;
				std::array<std::string, 5> params = createRequestParams(vm, "");
				auto request = RequestFactory::createRequest(id++, params);

				BOOST_LOG_SEV(logger, debug)<< request->toString();

				if (vm.count("client-numbers"))
				{
					Admin admin(config, extractNumbers(vm["client-numbers"].as<std::string>()), vm["timeout"].as<int>());
					admin.sendToClients(std::move(request));
				}
				else
				{
					Admin admin(config, vm["timeout"].as<int>());
					admin.sendToClients(std::move(request));
				}

			}
			catch (BaseException& e)
			{
				BOOST_LOG_SEV(logger, error)<< "Connection Error.\n";
				return EXIT_FAILURE;
			}
		}
		else
		{
			throw boost::program_options::error_with_no_option_name(
					"Neither of append, delete, insert or list option was chosen");
		}
	}
	catch (po::error& e)
	{
		std::cerr << "Options error: " << e.what() << ".\n";
		std::cerr << "Use --help option to see help.\n";
		return EXIT_FAILURE;
	}
	catch (std::exception& e)
	{
		BOOST_LOG_SEV(logger, error)<< "Error occurred: " << e.what() << ".";
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}


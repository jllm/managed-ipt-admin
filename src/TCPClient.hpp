#pragma once

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <algorithm>

#include "../managed-ipt-common/src/exceptions/AcceptException.hpp"
#include "../managed-ipt-common/src/exceptions/BaseException.hpp"
#include "../managed-ipt-common/src/exceptions/BindException.hpp"
#include "../managed-ipt-common/src/exceptions/ConnectException.hpp"
#include "../managed-ipt-common/src/exceptions/ListenException.hpp"
#include "../managed-ipt-common/src/exceptions/NetException.hpp"
#include "../managed-ipt-common/src/exceptions/ReadException.hpp"
#include "../managed-ipt-common/src/exceptions/SocketException.hpp"
#include "../managed-ipt-common/src/exceptions/WriteException.hpp"

class TCPClient
{
private:
	static const int BUFFER_SIZE = 10000;
	char readBuffer[BUFFER_SIZE];
	std::string ipAddress;
	int portNumber;

	int doConnection();

public:
	TCPClient(const std::string&, int);
	std::string sendAndGetReply(const std::string&, const int);
};

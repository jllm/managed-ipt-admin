/*
 * Setup.hpp
 *
 *  Created on: 2 maj 2014
 *      Author: Jakub Szuppe
 */

#pragma once

#include <stdexcept>
#include <unistd.h>
#include <cstdio>
#include <fstream>
#include "../managed-ipt-common/src/KeyGen.hpp"

/**
 * Klasa produkująca domyślne ustawienia do plików konfiguracyjnych, klucz publiczny
 * i prywatny, pusty plik z listą klientów.
 *
 * Domyślnie nie nadpisuje plików jeżeli istnieją.
 */
class Setup
{
	private:
		bool force;

		static std::string DEFAULT_PRIVATE_KEY_FILE_NAME;
		static std::string DEFAULT_PUBLIC_KEY_FILE_NAME;
		static std::string DEFAULT_CLIENTS_FILE_NAME;

	public:
		Setup(const bool _force = false) :
				force(_force)
		{

		}
		void run();
};


/*
 * Config.hpp
 *
 *  Created on: 3 maj 2014
 *      Author: jakub
 */

#pragma once

#include <string>

class Config
{
	private:

		const std::string adminPrivKeyFile;
		const std::string adminPubKeyFile;
		const std::string clientsListFile;

	public:

		Config(const std::string&, const std::string&, const std::string&);
		~Config();
		/**
		 * Wczytuje konfiguracje z pliku.
		 * @param file - plik konfiguracji.
		 * @return obiekt konfiguracji.
		 */
		static Config load(const std::string& file);
		std::string getClientsListFile() const;
		std::string getAdminPrivateKeyFile() const;
		std::string getAdminPublicKeyFile() const;
};


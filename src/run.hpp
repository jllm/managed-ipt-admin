/*
 * run.hpp
 *
 *  Created on: 2 maj 2014
 *      Author: jakub
 */

#pragma once

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <stdexcept>
#include <algorithm>
#include <functional>
#include <boost/algorithm/string.hpp>

#include "Admin.hpp"
#include "Setup.hpp"
#include "TCPClient.hpp"
#include "ClientsRetriever.hpp"
#include "../managed-ipt-common/src/common.hpp"
#include "../managed-ipt-common/src/defines.hpp"
#include "../managed-ipt-common/src/RequestFactory.hpp"

#include <boost/program_options.hpp>

struct iptArgs
{
		enum command
		{
			append,
			del,
			insert
		};

		std::string table;
		std::string chain;
		std::string options;
};

std::array<std::string, 5> createRequestParams(boost::program_options::variables_map vm, std::string sign);
void check_conflicts(const boost::program_options::variables_map& vm);
void listClients(const std::string&);
std::unique_ptr<std::vector<int>> extractNumbers(const std::string&);
int run(int argc, char* argv[]);

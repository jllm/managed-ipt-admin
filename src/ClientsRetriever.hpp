#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <cryptopp/rsa.h>
#include <cryptopp/files.h>
#include <cryptopp/base64.h>
#include "ClientEntry.hpp"

/**
 * Pobiera dane agentów z pliku
 */
class ClientsRetriever
{
	private:
		const std::string file;

	public:
		ClientsRetriever(const std::string&);
		std::unique_ptr<std::vector<ClientEntry>> retrieve();
};

#include "Admin.hpp"

Admin::Admin(const Config& config, std::unique_ptr<std::vector<int>> clientNumbers, const int timeout)
:
		privateKey(KeyLoader::readRSAPrivateKey(config.getAdminPrivateKeyFile()))
				, isClientNumbersUsed(true)
				, clientNumbers(std::move(clientNumbers))
				, timeout(timeout)
{
	std::string clientsFile = config.getClientsListFile();
	ClientsRetriever cr(clientsFile);
	clients = cr.retrieve();
}

Admin::Admin(const Config& config, const int timeout)
:
		  Admin(config, nullptr, timeout)
{
	isClientNumbersUsed = false;
}

void Admin::sendToClientsWorker(std::string& requestStr,
		std::atomic<unsigned int>& index, std::vector<ClientEntry>& clientsList,
		std::mutex& responseListMutex, std::vector<std::pair<ClientEntry, std::string>>& responses)
{
	unsigned int idx;
	unsigned int clientsListSize = clientsList.size();
	// Atomowo wczytuje nowy indeks i zwiększam o jeden
	while ((idx = index.fetch_add(1)) < clientsListSize)
	{
		const auto& client = clientsList[idx];
		try
		{
			// Połączenie z klientem, zapytanie, odpowiedź
			TCPClient tcpClient(client.getIp(), client.getPort());
			std::string response = tcpClient.sendAndGetReply(requestStr, timeout);

			// Wpisanie wyniku
			responseListMutex.lock();
			responses.push_back(std::make_pair(client, std::move(response)));
			responseListMutex.unlock();
		} catch (const BaseException& e)
		{
			BOOST_LOG_SEV(logger, warning)<< "Connection error. Client: "
			<< client.getIp() << " " << client.getPort();
		}
	}
}

void Admin::sendToClients(std::unique_ptr<Request> request)
{
	// Trochę dziwnie tutaj ten podpis wygląda, ale klucz jest już gotowy.
	request->sign(privateKey);

	// List par wpis klienta i jego odpowiedź
	std::vector<std::pair<ClientEntry, std::string>> responses;
	// Lista klientów, których należy użyć (nie zawsze wszyscy)
	// getClients() filtruje.
	auto clientsList = getClients();
	// Rezerwujemy sobie miejsce w wektorze
	responses.reserve(clientsList.size());

	// Wektor wątków, które obsługują klientów.
	std::vector<std::thread> workers;
	// Wątki w liczbach:
	// 1. Minimalnie jeden
	// 2. Maksymalnie MAX_THREADS_NO
	// 3. WPP.: <liczba klientów>
	int workersNo = std::max(MAX_THREADS_NO, (const int)clientsList.size());
	workers.reserve(workersNo);

	// Wyciągamy string z request
	std::string requestString = request->toStringBase64();
	// Atomowy index, którym będą posługiwać się wątki
	std::atomic<unsigned int> index(0);
	// Mutex do wektora odpowiedzi od klientów
	std::mutex responseVecMutex;

	// Startuje wątki.
	for (int i = 0; i < workersNo; i++)
	{
		workers.push_back(
				std::thread(&Admin::sendToClientsWorker, this,
						std::ref(requestString),
						std::ref(index),
						std::ref(clientsList),
						std::ref(responseVecMutex),
						std::ref(responses)));
	}
	// Czekam na zakończenie
	std::for_each(workers.begin(), workers.end(), [](std::thread &t)
	{
		t.join();
	});

	// Prezentacja otrzymanych odpowiedzi.
	for (auto& clientResponse : responses)
	{
		const ClientEntry& client = clientResponse.first;
		auto& responseString = clientResponse.second;

		try
		{
			// Zamiana na obiekt odpowiedzi.
			Response response = Response::createResponse(responseString);

			BOOST_LOG_SEV(logger, debug)
			<< client.getIp() << " " << client.getPort() << " : "
			<< response.toString();

			// Sprawdzenie podpisu.
			if (!response.checkSign(client.getPublicKey()))
			{
				std::logic_error("Invalid signature");
			}

			// Zwrócenie wyników wykonania.
			BOOST_LOG_SEV(logger, info)
			<< client.getIp() << " " << client.getPort() << " : \n" << response.getOutput();
		}
		catch (const std::logic_error& e)
		{
			BOOST_LOG_SEV(logger, warning)<< "Invalid signature. Client: "
			<< client.getIp() << " " << client.getPort();
		}
		catch (const std::exception& e)
		{
			BOOST_LOG_SEV(logger, warning) << "Error occured:" << e.what() << ". Client: "
			<< client.getIp() << " " << client.getPort();
		}
	}
}

// TODO: z unique_ptr mam segfault, pewnie trzeba shared_ptr użyć.
std::vector<ClientEntry> Admin::getClients()
{
	if (isClientNumbersUsed)
	{
		std::vector<ClientEntry> filteredClients;
		for (int clientNumber : *clientNumbers)
		{
			filteredClients.push_back(clients->at(clientNumber - 1));
		}
		return filteredClients;
	}
	else
	{
		return *clients;
	}
}

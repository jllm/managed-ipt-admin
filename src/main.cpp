#include <iostream>
#include <exception>

#include "run.hpp"
#include "../managed-ipt-common/src/common.hpp"
#include "../managed-ipt-common/src/defines.hpp"

namespace
{
	Logger logger;
}

int main(int argc, char* argv[])
{
	try
	{
		return run(argc, argv);
	}
	catch (std::exception& e)
	{
		BOOST_LOG_SEV(logger, fatal)<< "Unhandled Exception reached the top of main: " << e.what() << ".";
		BOOST_LOG_SEV(logger, fatal) << "Application will now exit.";
		return EXIT_FAILURE;
	}
	catch (...)
	{
		BOOST_LOG_SEV(logger, fatal) << "Unhandled exception of unknown type reached the top of main.";
		BOOST_LOG_SEV(logger, fatal) << "Application will now exit.";
		return EXIT_FAILURE;
	}
}

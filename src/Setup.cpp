/*
 * Setup.cpp
 *
 *  Created on: 2 maj 2014
 *      Author: jakub
 */

#include "Setup.hpp"

std::string Setup::DEFAULT_PRIVATE_KEY_FILE_NAME = std::string("rsa");
std::string Setup::DEFAULT_PUBLIC_KEY_FILE_NAME = std::string("rsa.pub");
std::string Setup::DEFAULT_CLIENTS_FILE_NAME = std::string("clients");

void Setup::run()
{
	if ((access(DEFAULT_CLIENTS_FILE_NAME.c_str(), F_OK) < 0)
			|| this->force == true)
	{
		std::fstream clientsFile(DEFAULT_CLIENTS_FILE_NAME, std::ios_base::out | std::ios_base::trunc);
		clientsFile.close();
	}

	// Jeżeli nie ma obu plików z kluczami, lub jest force to nowe klucze
	bool privKeyFileDoesntExist, pubKeyFileDoesntExist;
	if (
			(
					(pubKeyFileDoesntExist = (access(DEFAULT_PUBLIC_KEY_FILE_NAME.c_str(), F_OK) < 0))
					&&
					(privKeyFileDoesntExist = (access(DEFAULT_PRIVATE_KEY_FILE_NAME.c_str(), F_OK) < 0))
			)
			|| this->force == true
		)
	{
		// Usuwanie jeżeli są.
		if (!pubKeyFileDoesntExist)
		{
			remove(DEFAULT_PUBLIC_KEY_FILE_NAME.c_str());
		}
		if (!privKeyFileDoesntExist)
		{
			remove(DEFAULT_PRIVATE_KEY_FILE_NAME.c_str());
		}

		KeyGen keygen;
		auto keys = keygen.generate();
		keygen.savePrivateKey(DEFAULT_PRIVATE_KEY_FILE_NAME, keys->second);
		keygen.savePublicKey(DEFAULT_PUBLIC_KEY_FILE_NAME, keys->first);
	}
}

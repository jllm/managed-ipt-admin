#ifndef ADMIN_HPP_
#define ADMIN_HPP_

#include <string>
#include <list>
#include <thread>
#include <memory>
#include <mutex>
#include <atomic>
#include <algorithm>
#include <stdexcept>

#include "ClientsRetriever.hpp"
#include "ClientEntry.hpp"
#include "Config.hpp"
#include "TCPClient.hpp"
#include "../managed-ipt-common/src/Request.hpp"
#include "../managed-ipt-common/src/Response.hpp"
#include "../managed-ipt-common/src/KeyLoader.hpp"
#include "../managed-ipt-common/src/common.hpp"

#define MAX_THREADS_NO 100

class Admin
{
	private:

		/** Lista klientów */
		std::unique_ptr<std::vector<ClientEntry>> clients;
		/** Klucz prywatny */
		const CryptoPP::RSA::PrivateKey privateKey;
		/** Klucz publiczny */
		const CryptoPP::RSA::PublicKey publicKey;
		/** Czy używać wektora klientów */
		bool isClientNumbersUsed;
		/** Numery klientów do których należy wysłać polecenie */
		std::unique_ptr<std::vector<int>> clientNumbers;
		/** Czas oczekiwania na odpowiedź od jednego agenta w sekundach*/
		const int timeout;

		Logger logger;

		/** Zwraca klientów do których należy wysłać polecenie */
		std::vector<ClientEntry> getClients();

	public:

		Admin(const Config&, const int);
		Admin(const Config&, std::unique_ptr<std::vector<int>>, const int);
		void sendToClients(std::unique_ptr<Request>); // TODO: zamienić na const po updacie.

	private:

		void sendToClientsWorker(std::string& requestStr,
				std::atomic<unsigned int>& index, std::vector<ClientEntry>& clientsList,
				std::mutex& responseListMutex, std::vector<std::pair<ClientEntry, std::string>>& responses);
};

#endif
